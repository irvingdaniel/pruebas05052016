package mx.com.prueba05052016.prueba05052016;

import java.util.ArrayList;

public class ListaDeCadenasApp {
	ArrayList<String> myList;
	String[] nombres;
	ArrayList<String> myList2;
	String[] nombres2;

	public static void main(final String[] args) {
		final ListaDeCadenasApp obj1 = new ListaDeCadenasApp();
		final String[] nombres = { "Jose", "María", "Julieta", "Armando",
				"Esteban", "Laura", "Estefania" };
		obj1.myList2 = new ArrayList<String>();
		obj1.nombres2 = new String[] { "Jose", "María", "Julieta", "Armando",
				"Esteban", "Laura", "Estefania" };

		final ListaDeCadenas miLista1 = new ListaDeCadenas(obj1.myList2,
				nombres);
		miLista1.creaLlenaListaCadena(obj1.myList2, obj1.nombres2);
		miLista1.imprimeListaCadena(obj1.myList2);
	}
}

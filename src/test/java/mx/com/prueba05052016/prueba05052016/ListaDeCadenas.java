package mx.com.prueba05052016.prueba05052016;

import java.util.ArrayList;

public class ListaDeCadenas {
	public ArrayList<String> miLista;
	public String[] cadenas;

	public ListaDeCadenas(final ArrayList<String> list, final String[] cade) {
		miLista = list;
		cadenas = cade;
	}

	public void creaLlenaListaCadena(ArrayList<String> lista,
			final String[] nombres) {
		lista = new ArrayList<String>();
		for (int i = 0; i < nombres.length - 1; i++) {
			lista.add(nombres[i]);
			// System.out.println(lista.get(i));
		}
		imprimeListaCadena(lista);
	}

	public void imprimeListaCadena(final ArrayList<String> lista) {
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}
	}
}
